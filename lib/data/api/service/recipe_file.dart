import 'dart:convert';
import 'package:flutter/services.dart';
import '../model/api_recipe.dart';


class RecipeFileService {

  String databaseFilePath = '';
  List<dynamic> database = [];

  RecipeFileService({ required this.databaseFilePath }){
    this.loadData();
  }

  Future<bool> loadData() async {
    database = await _readJsonFile(databaseFilePath);
    return true;
  }

  Future<List<dynamic>> _readJsonFile(String filePath) async {
    String input = await rootBundle.loadString(filePath);
    var map = jsonDecode(input);
    return map["recipes"];
  }

  ApiRecipe? getRecipe({
    required int id})
  {
    var dataJson;
    for (int i = 0; i < database.length; i++) {
      if (database[i]["id"] == id) {
        dataJson = database[i];
        break;
      }
    }
    return (dataJson != null) ? ApiRecipe.fromApi(dataJson) : null;
  }

  List<ApiRecipe> getAll(){
    List<ApiRecipe> output = [];
    for (int i = 0; i < database.length; i++) {
      output.add(ApiRecipe.fromApi(database[i]));
    }
    return output;
  }
}


class ApiRecipe {
  late final String title;
  late final int id;
  late final String imageUri;
  final List<Map<String, dynamic>> ingredients = [];
  final List<Map<String, dynamic>> steps = [];

  ApiRecipe.fromApi(Map<String, dynamic> map){
    title = map['title'];
    id = map['id'];
    imageUri = map['imageUri'];

    for (int i = 0; i < map["ingredients"].length; i++) {
      ingredients.add(map["ingredients"][i]);
    }

    for (int i = 0; i < map["steps"].length; i++) {
      steps.add(map["steps"][i]);
    }
  }

  @override
  String toString() {
    return "Title: ${title}, ID: ${id}\n";
  }
}

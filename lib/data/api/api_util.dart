import 'package:otusfood/data/api/model/api_recipe.dart';
import 'package:otusfood/data/api/service/recipe_file.dart';
import '../../domain/model/recipe.dart';
import '../mapper/recipe_mapper.dart';


class ApiUtil {
  final RecipeFileService _recipeFileService;

  ApiUtil(this._recipeFileService);

  Recipe? getRecipe({
    required int id
  }) {
    final result = _recipeFileService.getRecipe(id: id);
    return result != null ? RecipeMapper.oneFromApi(result) : null;
  }

  List<Recipe> getRecipes(){
    List<Recipe> output = [];
    List<ApiRecipe> allRecipesApi = _recipeFileService.getAll();
    output = RecipeMapper.manyFromApi(allRecipesApi);
    return output;
  }
}

import '../../domain/model/recipe.dart';
import '../api/model/api_recipe.dart';


class RecipeMapper {

  static Recipe oneFromApi(ApiRecipe recipe) {

    var output = Recipe(title: recipe.title,
                        imageUri: recipe.imageUri,
                        ingredients: [],
                        steps: []);
    return output;
  }

  static List<Recipe> manyFromApi(List<ApiRecipe> recipes) {
    List<Recipe> output = [];
    for (int i = 0; i < recipes.length; i++) {
      output.add(RecipeMapper.oneFromApi(recipes[i]));
    }
    return output;
  }
}

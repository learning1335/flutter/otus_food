import '../../domain/model/recipe.dart';
import '../../domain/repository/recipes.dart';
import '../api/api_util.dart';


class RecipeDataRepository extends RecipeRepository {

  final ApiUtil _apiUtil;

  RecipeDataRepository(this._apiUtil);

  @override
  Recipe? getRecipeById({required int id}) {
    return _apiUtil.getRecipe(id: id);
  }

  @override
  List<Recipe> getRecipes() {
    return _apiUtil.getRecipes();
  }

  @override
  int totalItems(){
    return _apiUtil.getRecipes().length;
  }
}

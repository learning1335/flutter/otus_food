
class Ingredient {
  final String name;
  final String quantity;

  Ingredient({
    required this.name,
    required this.quantity
  });
}


class StepPrepare {
  final String name;
  final int duration; // sec

  StepPrepare({
    required this.name,
    required this.duration
  });
}


class Recipe {
  final String title;
  final String imageUri;
  final List<Ingredient> ingredients;
  final List<StepPrepare> steps;

  Recipe({
    required this.title,
    required this.imageUri,
    required this.ingredients,
    required this.steps
  });


}
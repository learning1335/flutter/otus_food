import '../model/recipe.dart';


abstract class RecipeRepository {

  Recipe? getRecipeById({
    required int id
  });

  List<Recipe> getRecipes();

  int totalItems();

}


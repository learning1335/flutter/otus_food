import 'package:flutter/material.dart';
import '../domain/model/recipe.dart';
import '../domain/repository/recipes.dart';
import '../internal/dependencies/repository_module.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  late RecipeRepository _recipeRepository;

  @override
  initState() {
    super.initState();
    _recipeRepository = RepositoryModule.recipeRepository(databaseFilePath: 'assets/data/database.json');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("OTUS Recipes"),
        ),
        body: ListView.builder(
            itemCount: _recipeRepository.totalItems(),
            itemBuilder: (BuildContext context, int index) {
            return _get_recipe(_recipeRepository.getRecipes()[index]);
        },
        )
    );
  }

  Widget _get_recipe(Recipe recipe){
    return Column(
      children: [
        Text(recipe.title, style: const TextStyle(fontWeight: FontWeight.bold)),
        Image.asset(recipe.imageUri, width: 270, height: 190, fit:BoxFit.fill)
      ],
    );
  }
}



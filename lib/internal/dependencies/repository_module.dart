import '../../data/repository/recipes.dart';
import '../../domain/repository/recipes.dart';
import 'api_module.dart';


class RepositoryModule {

  static RecipeRepository recipeRepository({ required databaseFilePath}) {
    RecipeRepository _recipeRepository = RecipeDataRepository(
            ApiModule.apiUtil(databaseFilePath: databaseFilePath),
    );
    return _recipeRepository;
  }
}

import '../../data/api/api_util.dart';
import '../../data/api/service/recipe_file.dart';


class ApiModule {

  static ApiUtil apiUtil({ required databaseFilePath}) {

    var _apiUtil = ApiUtil(RecipeFileService(databaseFilePath: databaseFilePath));

    return _apiUtil;
  }
}